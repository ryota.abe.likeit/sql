﻿CREATE DATABASE a DEFAULT CHARACTER SET utf8;

use a;
CREATE TABLE item_category(
category_id int PRIMARY KEY AUTO_INCREMENT,
category_name varchar(256) NOT NULL
);

